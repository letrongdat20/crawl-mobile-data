import scrapy
import json
from selenium import webdriver
from scrapy import Selector
from webdriver_manager.chrome import ChromeDriverManager
from bs4 import BeautifulSoup

class NguyenKimSpider(scrapy.Spider):
    name = "nguyenkim_product"
    list_product = []
    def __init__(self):
        self.driver = webdriver.Chrome(ChromeDriverManager().install())
    def start_requests(self):
        urls = ['https://www.nguyenkim.com/dien-thoai-di-dong/page-{}/',
                "https://www.nguyenkim.com/may-tinh-xach-tay/page-{}/"]
        for i in range(1, 6):
            yield scrapy.Request(url=urls[0].format(i), callback=self.parse_path)
        for i in range(1, 3):
            yield scrapy.Request(url=urls[1].format(i), callback=self.parse_path)

    def parse_path(self, response):
        li = response.css("div.product-image a").xpath('@href').getall()
        for item in li:
            if item not in self.list_product:
                self.list_product.append(item)
                yield scrapy.Request(url=item, callback=self.parse_product)
    
    def parse_product(self, res):
        phone = dict()
        self.driver.get(res.url)
        response = Selector(text=self.driver.page_source)
        phone['attribute'] = self.driver.execute_script("return showProductFeature")
        review = response.xpath("//script[@type='application/ld+json']/text()").getall()
        if review is not None:
            phone['review'] = review[2]
        else: 
            phone['review'] = r'{}'
        with open('./product.txt', 'a', encoding='utf-8') as f:
            f.write(", \n")
            json.dump(phone, f, ensure_ascii=False)
        