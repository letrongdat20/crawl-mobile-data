from requests_html import HTMLSession
import json

header = {
    'Accept': r'*/*',
    'X-Requested-With': r'XMLHttpRequest',
    'User-Agent': r'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36',
    'sec-ch-ua-platform': r"Windows"
}

list_product = []


def parse_path(response):
    li = response.html.xpath('//a[@class="main-contain"]/@href')
    for item in li:
        url = "https://www.thegioididong.com" + item
        if url not in list_product:
            list_product.append(url)
            urls.append(url)


start_url = ['https://www.thegioididong.com/dtdd#c=42&o=9&pi=6',
             r"https://www.thegioididong.com/laptop#c=44&o=9&pi=10"]
urls = []


def write_path():
    with open('./data/product-link.txt', "a") as f:
        s = "\n".join(urls) + "\n"
        f.write(s)


def parse_product(response):
    phone = dict()
    # name
    phone['property'] = response.html.xpath(
        "//input[@id='jsonProductGTM']/@value")
    review_list = response.html.find("div.article__content")
    if len(review_list) > 0:
        phone['review'] = review_list[0].text
    else:
        phone['review'] = ""
    with open('./data/product.txt', 'a', encoding='utf-8') as f:
        f.write(", \n")
        json.dump(phone, f, ensure_ascii=False)

    orthers = response.html.xpath("//a[@class='box03__item']/@href")
    for orther in orthers:
        url = 'https://www.thegioididong.com' + orther
        if url not in list_product:
            list_product.append(url)
            urls.append(url)

if __name__ == "__main__":
    for url in start_url:
        print(url)
        session = HTMLSession()
        res = session.get(url, headers=header)
        res.html.render(timeout=20)
        parse_path(res)
        write_path()

    while len(urls) > 0:
        url = urls.pop(0)
        session = HTMLSession()
        res = session.get(url, headers=header)
        print(url)
        parse_product(response=res)
