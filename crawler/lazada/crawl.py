from requests_html import HTMLSession
import json
import requests
import time
import random

header = {
    'Accept': r'*/*',
    'X-Requested-With': r'XMLHttpRequest',
    'User-Agent': r'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36',
    'sec-ch-ua-platform': r"Windows"
}

product_url = "https://www.lazada.vn/products/product-i{id}-s{sku}.html"

product_id_file = "./data/product_id.txt"
product_data_file = "./data/product.txt"
laptop_page_url = ['https://www.lazada.vn/dien-thoai-di-dong/?ajax=true&page={page}',
                   'https://www.lazada.vn/laptop/?ajax=true&page={page}']

path_script = '''
    () => {
        return window.pageData;
    }
'''

proxies = [{
    'http': '169.57.157.148:8123',
    'https': '169.57.157.148:8123',
},
{
    'http': '162.159.242.242:80',
    'https': '162.159.242.242:80',
},
{
    'http': '159.8.114.37:80',
    'https': '159.8.114.37:80',
},
{
    'http': '162.159.251.122:80',
    'https': '162.159.251.122:80',
},
{
    'http': '162.159.244.149:80',
    'https': '162.159.244.149:80',
},
{
    'http': '162.159.248.7:80',
    'https': '162.159.248.7:80',
},
]


def crawl_product_id(path):
    product_list = set()
    i = 8
    while (True):
        np = path.format(page=i)
        print(np)
        time.sleep(3)
        response = requests.get(np, headers=header)
        try:
            if response.status_code != 200:
                print(response.status_code)
                continue
            # res = response.html.render(script=path_script, timeout=20)
            res = json.loads(response.text)
            product_box = res['mods']['listItems']

            if (len(product_box) == 0):
                break
            for product in product_box:
                id = product['itemId']
                sku = product['skuId']
                product_list.add(id + " " + sku)
                print(id + " " + sku)
        except Exception:
            continue
        i += 1
    return product_list, i


def save_product_id(product_list=[]):
    with open(product_id_file, "a") as f:
        str = "\n".join(product_list) + "\n"
        f.write(str)


script = '''
    () => {
        return __moduleData__;
    }
'''


def crawl_product(product_list):
    product_detail_list = []
    for product_id, s in product_list:
        session = HTMLSession()
        try:
            print(product_url.format(id=product_id, sku=s))
            res = session.get(product_url.format(
                id=product_id, sku=s), headers=header)
            time.sleep(10)
            if (res.status_code == 200):
                data = res.html.render(script=script, timeout=60)
                info = res.html.xpath(
                    "//script[@type='application/ld+json']", first=True)
                if info is not None:
                    info = info.text
                product_detail_list.append(
                    {'id': product_id, 'sku': s, 'data': data, 'description': info})
                print("Crawl product: ", product_id, ": ", res.status_code)
            else:
                print("faile Crawl product: " +
                      product_id + ": " + res.status_code)

        except Exception:
            print("faile to parse: ", product_id, ": ", res.status_code)
    return product_detail_list


def save_raw_product(product_detail_list=[]):
    for product in product_detail_list:
        with open(product_data_file, "a", encoding="utf-8") as f:
            f.write(",")
            json.dump(product, f,  ensure_ascii=False)


def crawl_path(prox):
    # crawl product id
    while len(laptop_page_url) > 0:
        path = laptop_page_url.pop(0)
        product_list_set, page = crawl_product_id(path, prox)
        product_list = [str(p) for p in product_list_set]
        print("No. Product ID: ", len(product_list))

        # save product id for backup
        save_product_id(product_list)


def read_id():
    phone_path = []
    with open('./data/product_id.txt') as f:
        ls = f.readlines()
    for l in ls:
        li = l.split()
        phone_path.append((li[0], li[1]))
    return phone_path


if __name__ == "__main__":
    # crawl_path()
    list_product = read_id()
    # crawl detail for each product id
    product_list = crawl_product(list_product)

    # # save product detail for backup
    save_raw_product(product_list)
