import scrapy
import json
from selenium import webdriver
from scrapy import Selector
from webdriver_manager.chrome import ChromeDriverManager
from bs4 import BeautifulSoup

class FptSpider(scrapy.Spider):
    name = "fpt_product"
    list_product = []
    def __init__(self):
        self.driver = webdriver.Chrome(ChromeDriverManager().install())
    def start_requests(self):
        urls = ['https://fptshop.com.vn/apiFPTShop/Product/GetProductList?brandAscii=&url=https:%2F%2Ffptshop.com.vn%2Fdien-thoai%3Fsort%3Dban-chay-nhat%26trang%3D12%26pagetype%3D1&s=fee2af5bd98388143b5be5a4ad776e54f3e875ca888c4d892d52ac80d97f668f',
                r"https://fptshop.com.vn/apiFPTShop/Product/GetProductList?brandAscii=&url=https:%2F%2Ffptshop.com.vn%2Fmay-tinh-xach-tay%3Fsort%3Dban-chay-nhat%26trang%3D6%26pagetype%3D1&s=f4f16ffa607a123e09687c7deca64afa7ef06d702e60149bd9a0b8eef962cbd6"]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse_path)

    def parse_path(self, response):
        data = json.loads(response.body)
        li = data['datas']['filterModel']['listDefault']['list']
        for item in li:
            url = "https://fptshop.com.vn/{type}/{name}".format(type=item['productType']['nameAscii'], name=item['nameAscii'])
            if url not in self.list_product:
                self.list_product.append(url)
                yield scrapy.Request(url=url, callback=self.parse_product)
    
    def parse_product(self, res):
        phone = dict()
        self.driver.get(res.url)
        response = Selector(text=self.driver.page_source)
        #name
        phone['name'] = response.css(".st-name::text").get()
        #price
        phone['price'] = response.css('div.st-price-main::text').get()
        #color
        colors = response.css("div.js--select-color-item")
        di = dict()
        for color in colors:
            di[color.css('p').attrib['title']] = color.css('img').attrib['src']
        phone['color'] = di
        #rating
        phone['rate'] = response.css('div.l-pd-rate span.badge::text').get()
            
        rate_star = response.css("div.point::text").get()
        
        #comment
        phone['comment'] = response.css('div.l-pd-comment span.badge::text').get()
        review_list = response.css(".st-pd-content").extract()
        if len(review_list) > 0:
            review = BeautifulSoup(review_list[0])
            phone['review'] = review.get_text()
        else:
            phone['review'] = ""
        # property
        rows = response.css("div.c-modal__content div.c-modal__row")
        pro = dict()
        for row in rows:
            title = row.css(".st-table-title::text").get()
            value = dict()
            lst = []
            trs = row.css("tr")
            for tr in trs:
                tds = tr.css('td::text').getall()
                if len(tds) == 2:
                    value[tds[0]] = tds[1]
                else:
                    lst.append(tds[0])
            lis = row.css("li::text").getall()
            for li in lis:
                lst.append(li.replace("\n", ''))
            if len(lst) > 0:
                value['thong tin them'] = lst
            pro[title] = value
            
        phone['property'] = pro
        with open('./product.txt', 'a', encoding='utf-8') as f:
            f.write(", \n")
            json.dump(phone, f, ensure_ascii=False)
        orthers = response.css(".js--select-item")
        lst = []
        for orther in orthers:
            url = 'https://fptshop.com.vn' + orther.attrib['href']
            if url not in self.list_product:
                self.list_product.append(url)
                yield scrapy.Request(url=url, callback=self.parse_product)