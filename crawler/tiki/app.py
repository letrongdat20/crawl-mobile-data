from requests_html import HTMLSession
import json

header = {
    'authority': 'tiki.vn',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Safari/537.36',
    'sec-ch-ua-platform': "Windows",
    'accept': 'application/json, text/plain, */*'
}
product_url = "https://tiki.vn/api/v2/products/{}"

product_id_file = "./data/product-id.txt"
product_data_file = "./data/product.txt"


def crawl_product_id(path):
    print(path)
    product_list = set()
    i = 1
    while (True):
        np = path.format(i)
        print(np)
        session = HTMLSession()
        response = session.get(np, headers=header)
        response.html.render(timeout=20)
        if response.status_code != 200:
            print(response.html.text)
        product_box = response.html.find(".product-item")

        if (len(product_box) < 70):
            break
        for product in product_box:
            json_content = product.attrs["data-view-content"]
            content = json.loads(json_content)
            product_list.add(content['click_data']['id'])
            print(content['click_data']['id'])

        i += 1

    return product_list, i


def save_product_id(product_list=[]):
    with open(product_id_file, "a") as f:
        str = "\n".join(product_list) + "\n"
        f.write(str)


def crawl_product(product_list=[]):
    product_detail_list = []
    for product_id in product_list:
        try:
            session = HTMLSession()
            response = session.get(product_url.format(product_id), headers=header)
            if (response.status_code == 200):
                product_detail_list.append(response.html.text)
                print("Crawl product: ", product_id, ": ", response.status_code)
            else:
                print("faile Crawl product: ", product_id, ": ", response.status_code)
        except Exception:
            print("faile Crawl product: ", product_id, ": ", response.status_code)
    return product_detail_list


def save_raw_product(product_detail_list=[]):
    for product in product_detail_list:
        e = json.loads(product)
        if not e.get("id", False):
            continue

        with open(product_data_file, "a", encoding="utf-8") as f:
            f.write(",")
            json.dump(e, f,  ensure_ascii=False)


def crawl_path():
    laptop_page_url = ['https://tiki.vn/dien-thoai-may-tinh-bang/c1789?page={}',
                       "https://tiki.vn/laptop/c8095?src=c.8095.hamburger_menu_fly_out_banner&_lc=&page={}"]
    # crawl product id
    list_product = []
    while len(laptop_page_url) > 0:
        path = laptop_page_url.pop(0)
        product_list_set, page = crawl_product_id(path)
        product_list = [str(p) for p in product_list_set]
        print("No. Product ID: ", len(product_list))

        # save product id for backup
        save_product_id(product_list)

def read_id():
    phone_path = set()
    with open('./data/product-id.txt') as f:
        ls =  f.readlines()
    for l in ls:
        phone_path.add(l.replace("\n", '').replace(" ", ''))
    return phone_path

if __name__ == "__main__":
    # crawl_path()
    list_product = read_id()
    # crawl detail for each product id
    product_list = crawl_product(list_product)

    # save product detail for backup
    save_raw_product(product_list)
