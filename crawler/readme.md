## Crawl fpt product
<pre><code>cd fpt_crawl
scrapy crawl fpt_product
</code></pre>
Data in file ./fpt_crawl/product.txt

## Crawl tiki product
<pre><code>cd tiki
python app.py
</code></pre>
Data in file ./tiki/data/product.txt

## Crawl tgdd product
<pre><code>cd tgdd_crawl
python tgdd_crawl.py
</code></pre>
Data in file ./tgdd_crawl/data/product.txt

## Crawl tgdd product
<pre><code>cd lazada
python crawl.py
</code></pre>
Data in file ./lazada/data/product.txt

## Crawl fpt product
<pre><code>cd nguyenkim_crawl
scrapy crawl nguyenkim_product
</code></pre>
Data in file ./nguyenkim_crawl/product.txt