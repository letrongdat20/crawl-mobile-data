# Preprocessing step for mobile data

## Prepare raw data 
Get raw data from [this GoogleDrive link](https://drive.google.com/file/d/1IeofjrIPr_Zgc1N_HQoTtDfgwHwck1RF/view?usp=sharing)

Then, unzip and place somewhere in your local filesystem. 

## Preprocessing

### Bind mount the rawdata folder to Hadoop cluster's namenode
Edit `docker-compose-hadoop-spark.yml` to bind mount your rawdata folder to somewhere in the namenode's container filesystem.

### Bind mount the preprocessing notebooks to Jupyterlab 
Edit `docker-compose-hadoop-spark.yml` to bind mount this `preprocesing` directory to somewher in the jupyterlab's container filesystem.

### Start Spark & Hadoop cluster
Using `docker-compose-hadoop-spark.yml` file:
```bash
docker-compose -f docker-compose-hadoop-spark.yml up -d
```

### Create necessary directories 
Create these directories in HDFS:
- `/users/root/raw` for storing raw data
- `/users/root/tmp` for intermediate data

### Upload raw data to HDFS 
Inside the namenode:
```bash
hdfs dfs -put /path/to/mounted/raw/data/dir/*.txt /users/root/raw
```

### Running preprocessing notebook
Inside the jupyterlab, copy the preprocessing notebooks to `/opt/workspace`.

Access jupyterlab through https://localhost:8888/lab.

Run these 4 notebooks to process raw data from 4 different sources:
- `fpt.ipynb`
- `nguyenkim.ipynb`
- `tgdd.ipynb`
- `tiki.ipynb`

Preprocessing outputs will be stored at HDFS `/users/root/tmp`.

### Bring down Hadoop & Spark cluster
```bash
docker-compose -f docker-compose-hadoop-spark.yml down
```

Data stored in HDFS will persist inside docker volumes.