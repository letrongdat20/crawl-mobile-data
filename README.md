## Run Spark, hadoop, kibana, hive
<pre><code>docker-compose up</code></pre>

## run hadoop, spark
<pre><code>docker-compose -f docker-compose-hadoop-spark.yml up</code></pre>

## run hadoop, kibana
<pre><code>docker-compose -f docker-compose-hadoop-kibana.yml up</code></pre>


hadoop name node: http://localhost:9870/
spark master: http://localhost:8080/
jupyter lab: http://localhost:8888/lab
hive server: http://localhost:10000
kibana: http://localhost:5601/
elasticsearch: http://localhost:9200/