# Processing step for mobile data

## Prerequisite
Complete the preprocessing step described at `../preprocessing`.

Raw data should reside at `/users/root/raw` in HDFS.
Intermediate data (after preprocessed) should reside at `/users/root/tmp` in HDFS.

## Processing

### Bind mount the processing notebooks to Jupyterlab
Edit `docker-compose-hadoop-spark.yml` to bind mount this `processing` directory to somewhere iin the jupyterlab's container filesystem.

### Start Spark & Hadoop cluster
Using `docker-compose-hadoop-spark.yml` file:
```bash
docker-compose -f docker-compose-hadoop-spark.yml up -d
```

### Running processing notebook
Inside the jupyterlab, copy the processing notebook to `/opt/workspace`.

Access jupyterlab through https://localhost:8888/lab.

Run the `processing.ipynb` notebook to process intermediate data.

Output (cleaned) data will be stored at HDFS `/users/root/data`.

### Bring down Hadoop & Spark cluster
```bash
docker-compose -f docker-compose-hadoop-spark.yml down
```

Data stored in HDFS will persist inside docker volumes.